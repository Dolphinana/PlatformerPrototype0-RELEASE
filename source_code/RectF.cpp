/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "RectF.h"

RectF::RectF(float left_in,float top_in,float right_in,float bottom_in)
	:
	left(left_in),
	top(top_in),
	right(right_in),
	bottom(bottom_in)
{}

RectF::RectF(const Vec2& topleft,const Vec2& bottomright)
	:
	RectF(topleft.x, topleft.y, bottomright.x, bottomright.y)
{}

RectF::RectF(const Vec2 & topleft, float width, float height)
	:
	RectF(topleft, Vec2(topleft.x + width, topleft.y + height))
{
}

RectF RectF::FromCenter(const Vec2& center, float halfWidth, float halfHeight)
{
	Vec2 half = Vec2(halfWidth, halfHeight);
	return RectF(center - half, center + half);
}

Vec2 RectF::GetCenter() const
{
	return Vec2((left + right)/2,(top + bottom)/2);
}


bool RectF::IsOverLappingWith(const RectF& rhs) const
{
	return left < rhs.right &&
		right > rhs.left &&
		top < rhs.bottom &&
		bottom > rhs.top;
}
