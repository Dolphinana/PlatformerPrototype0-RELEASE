/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "Player.h"
#include <cmath>


Player::Player(const Vec2& pos_in)
	:
	pos(pos_in),
	oldpos(pos),
	vel(0.0f, 0.0f),
	gameOver(false)
{
}


void Player::Init(const Vec2& pos_in)
{
	pos = pos_in;
	oldpos = pos;
	vel = Vec2(0.0f, 0.0f);
	gameOver = false;
}

void Player::Update(float fElapsedTime)
{
	was_on_floor = on_floor;
	oldpos = pos;
	if (walking)
	{
		walkTime += 60.f * fElapsedTime;
	}

	on_floor_at_all = false;
	if ((walkTime > 3.f || !on_floor) )
	{
		spriteFrame = 1;
		if (walkTime > 6.f)
			walkTime = 0.f;
	}
	else //if (!walking || !std::signbit(std::cos(walkTime)))
	{
		spriteFrame = 0;
	}
}

void Player::JumpStart()
{
	jumpTime = 0.0f;
	vel.y = -jumpPower;
}

void Player::JumpHold()
{
	if (jumpTime < 10.0f && !on_floor)
	{
		vel.y = -jumpPower;
	}
}

void Player::JumpRelease()
{
	if (vel.y < -jumpPower / 2)
	{
		vel.y = -jumpPower / 2;
	}
	jumpTime = 10.0f;
}

bool Player::GetGameover() const
{
	return gameOver;
}

void Player::SetGameover(bool gameOver_in)
{
	gameOver = gameOver_in;
}
