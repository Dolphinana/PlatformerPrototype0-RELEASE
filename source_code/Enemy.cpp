/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "Enemy.h"


Enemy0::Enemy0(const Vec2& pos_in, EnemyTypes type_in)
	:
	initialized(true),
	pos(pos_in),
	vel(60.0f, 0),
	alive(true),
	hp(1),
	enemytype(type_in)
{
}




void Enemy0::Init(const Vec2& pos_in, EnemyTypes type_in)
{
	initialized = true;
	pos = pos_in;
	vel = Vec2(60.0f, 0);
	alive = true;
	hp = 1;
	enemytype = type_in;
}

void Enemy0::Update(float fElapsedTime)
{
	if (hp <= 0 || enemytype == EnemyTypes::NoEnemyTypes)
	{
		alive = false;
	}

	if (alive)
	{
		oldpos = pos;
		on_floor_at_all = false;
		switch (enemytype)
		{
		case EnemyTypes::WalkyFroggy:
			vel.y += fallAcc * fElapsedTime;
			pos += vel * fElapsedTime;
			break;
		case EnemyTypes::QuickBird:
			pos += vel * fElapsedTime;
			break;
		}
		
	}
}

Enemy0::PlayerCollideTypes Enemy0::CollidePlayer(Player& player)
{
	if (alive)
	{
		RectF playerrect = player.GetRect();
		RectF playeroldrect = player.GetOldRect();

		RectF rect = GetRect();
		RectF oldrect = GetOldRect();

		if (rect.IsOverLappingWith(playerrect))
		{
			if (playerrect.bottom > rect.top && playeroldrect.bottom < oldrect.top)
			{
				player.vel.y = -player.jumpPower * 1.3f;
				TakeDamage(1);
				return PlayerCollideTypes::Stomped;
			}
			return PlayerCollideTypes::Overlapping;
		}
	}
	return PlayerCollideTypes::NoColTypes;
}

void Enemy0::TakeDamage(int damage_in)
{
	hp -= damage_in;
}

void Enemy0::FollowPlayer(const Player& player, float fElapsedTime)
{
	if (enemytype == EnemyTypes::QuickBird && !player.GetGameover())
	{
		vel += ((player.pos - pos).GetNormalized() * fElapsedTime) * 300;
		if (vel.x > flySpeedCap)
			vel.x = flySpeedCap;
		if (vel.y > flySpeedCap)
			vel.y = flySpeedCap;
		if (vel.x < -flySpeedCap)
			vel.x = -flySpeedCap;
		if (vel.y < -flySpeedCap)
			vel.y = -flySpeedCap;
	}
}
