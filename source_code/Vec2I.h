/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once 

class Vec2I
{
public:
	Vec2I() = default;
	Vec2I(int x_in, int y_in);

	Vec2I operator+(const Vec2I& rhs) const;
	Vec2I& operator+=(const Vec2I& rhs);

	Vec2I operator-(const Vec2I& rhs) const;
	Vec2I& operator-=(const Vec2I& rhs);

	Vec2I operator*(const float rhs) const;
	Vec2I& operator*=(const float rhs);
	Vec2I operator*(const int rhs) const;
	Vec2I& operator*=(const int rhs);

	Vec2I operator/(const float rhs) const;
	Vec2I& operator/=(const float rhs);
	Vec2I operator/(const int rhs) const;
	Vec2I& operator/=(const int rhs);


	int GetLength_sq() const;
	int GetLength() const;


public:
	int x;
	int y;
};
