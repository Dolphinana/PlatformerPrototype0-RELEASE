#include "Vec2I.h"
#include <cmath>
/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


// in Vim do %s"replace_me"\="im_replaced"

Vec2I::Vec2I(int x_in,int y_in)
	:
	x(x_in),
	y(y_in)
{
}

Vec2I Vec2I::operator+(const Vec2I& rhs) const
{
	return Vec2I(x + rhs.x,y + rhs.y);
}

Vec2I& Vec2I::operator+=(const Vec2I& rhs)
{
	return *this = *this + rhs;
}

Vec2I Vec2I::operator-(const Vec2I& rhs) const
{
	return Vec2I(x - rhs.x,y - rhs.y);
}

Vec2I& Vec2I::operator-=(const Vec2I& rhs)
{
	return *this = *this - rhs;
}

Vec2I Vec2I::operator*(const float rhs) const
{
	return Vec2I(int(float(x) * rhs),int(float(y) * rhs));
}

Vec2I& Vec2I::operator*=(const float rhs)
{
	return *this = *this * rhs;
}

Vec2I Vec2I::operator*(const int rhs) const
{
	return Vec2I(x * rhs,y * rhs);
}

Vec2I& Vec2I::operator*=(const int rhs)
{
	return *this = *this * rhs;
}

Vec2I Vec2I::operator/(const float rhs) const
{
	return Vec2I(int(float(x) / rhs),int(float(y) / rhs));
}

Vec2I& Vec2I::operator/=(const float rhs)
{
	return *this = *this / rhs;
}

Vec2I Vec2I::operator/(const int rhs) const
{
	return Vec2I(x / rhs,y / rhs);
}

Vec2I& Vec2I::operator/=(const int rhs)
{
	return *this = *this / rhs;
}

int Vec2I::GetLength_sq() const
{
	return x * x + y * y;
}

int Vec2I::GetLength() const
{
	return int(std::sqrt(GetLength_sq()));
}

