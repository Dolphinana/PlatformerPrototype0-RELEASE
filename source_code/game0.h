/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once

#define OLC_PGE_APPLICATION
#include "olcPGE/olcPixelGameEngine.h"

#define OLC_PGEX_SOUND
#include "olcPGE/olcPGEX_Sound.h"

//#define OLC_PGEX_AUDIOCONVERT
//#include "olcPGEX_AudioConverter.h"

#include "Camera.h"
#include "Enemy.h"
#include "Gun.h"
#include "Particle.h"
#include "Player.h"
#include "RectF.h"
#include "Vec2.h"
#include "Vec2I.h"
#include "Tile.h"
#include "AllLevels.h"

const int ScWidth = int(0xFF * 1.5);
const int ScHeight = 0xFF;

const float fallAcc = 600.f;
const float gravity = 375.0f;

class Game0 : public olc::PixelGameEngine
{
public:
	Game0()
	{
		// Application name
		sAppName = "Game0";
	}

public:
	void SoftReset();
	void HardReset_or_Create();
	bool OnUserCreate() override;
	bool OnUserUpdate(float fElapsedTime) override;
	bool OnUserDestroy();





	bool isJumpPressed = false;


	AllLevels allLevels;

	static constexpr int nMaxParticles = 200;
	Particle particles[nMaxParticles];
	Camera cam;




	Player player;
	std::unique_ptr<olc::Sprite> spritePlayer;
	std::unique_ptr<olc::Sprite> spriteTiles0;
	std::unique_ptr<olc::Sprite> spriteTitle;
	std::unique_ptr<olc::Sprite> spriteAlfabet;

	std::unique_ptr<olc::Sprite> spritePlayMode;
	int playModeSprWidth = 64;
	int playModeSprHeight = 12;


	int fontSize = 8;
	float fontNum = 0;

	static constexpr int nEnemy0 = 20;
	Enemy0 enemy0[nEnemy0];

	struct SoundList
	{
		int lvl1escape;
		int intro_lvl1escape;
		int loop_lvl1escape;
		int shoot;
		int playerjump;
		int enemydamage;
		int playerdamage;
		int explosion0;
	};

	SoundList sounds;
	bool audioInitialized = false;


	int escapePress = 0;
	float timeUntilCancel = 0;
};
