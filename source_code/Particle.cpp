/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "Particle.h"

Particle::Particle(const Vec2& pos_in, const Vec2& vel_in, bool wrapScreen_in )
	:
	pos(pos_in),
	vel(vel_in),
	wrapScreen(wrapScreen_in),
	acc(Vec2(0.0f,0.0f))
{
}

Particle::Particle(const Vec2& pos_in, const Vec2& vel_in, const Vec2& acc_in, bool wrapScreen_in)
	:
	pos(pos_in),
	vel(vel_in),
	wrapScreen(wrapScreen_in),
	acc(acc_in)
{
}

void Particle::Update(float fElapsedTime, const Vec2& screenSize)
{
	vel += acc * fElapsedTime;
	pos += vel * fElapsedTime;

	if (wrapScreen)
	{
		if (pos.x < 0.0f)
		{
			pos.x = screenSize.x - 1;
		}
		else if (pos.x >= screenSize.x)
		{
			pos.x = 0.0f;
		}
		if (pos.y < 0.0f)
		{
			pos.y = screenSize.y - 1;
		}
		else if (pos.y >= screenSize.y)
		{
			pos.y = 0.0f;
		}
	}
}
