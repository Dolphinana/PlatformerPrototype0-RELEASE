/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "Gun.h"
#include "Player.h"
#include <cmath>
#include "Enemy.h"

Gun::Gun(int damage_in)
	:
	damage(damage_in)
{
}

void Gun::Update(float fElapsedTime)
{
	if (bullet[0].shoot)
	{
		bullet[0].shootTime += 1.0f * fElapsedTime;
		if (bullet[0].shootTime >= Gun::maxShootTime) 
		{
			bullet[0].Reset();
		}

		switch (gunType)
		{
		case GunTypes::Ordinary:
			bullet[0].pos += bullet[0].vel * fElapsedTime;
		case GunTypes::Directioner:
			bullet[0].pos += bullet[0].vel * fElapsedTime;

		}
	}
}

bool Gun::DoShoot(const Player& player)

{
	bullet[0].shoot = true;
	bullet[0].pos = player.pos;
	bullet[0].shootTime = 0.0f;
	switch (gunType)
	{
	case GunTypes::Ordinary:
		switch (player.looking)
		{
		case Player::LookingType::Up:
			bullet->vel.y = -bullet[0].speed;
			bullet->vel.x = 0;
			break;
		case Player::LookingType::Down:
			bullet->vel.y = bullet[0].speed;
			bullet->vel.x = 0;
			break;
		default:
			if (player.turnedRight)
			{
				bullet->vel.x = bullet[0].speed;
				bullet->vel.y = 0;
			}
			else
			{
				bullet->vel.x = -bullet[0].speed;
				bullet->vel.y = 0;
			}
			break;
		}

		break;
	case GunTypes::Directioner:
		bullet[0].vel = player.vel;
		break;

	}
	return true;
}

bool Gun::DoEnemyDamage(Enemy0& enemy)
{
	if ( bullet[0].GetRect().IsOverLappingWith(enemy.GetRect()) && bullet[0].shoot && enemy.alive)
	{
		enemy.TakeDamage(damage);
		bullet[0].Reset();
		return true;
	}

	return false;
}

void Gun::Bullet::Reset()
{
	shoot = false;
	shootTime = 0.0f;
}
