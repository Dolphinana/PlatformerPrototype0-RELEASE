/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "Vec2.h"
#include <cmath>

Vec2::Vec2(float x_in,float y_in)
	:
	x(x_in),
	y(y_in)
{
}

Vec2 Vec2::operator+(const Vec2& rhs) const
{
	return Vec2(x + rhs.x,y + rhs.y);
}

Vec2& Vec2::operator+=(const Vec2& rhs)
{
	return *this = *this + rhs;
}

Vec2 Vec2::operator-(const Vec2& rhs) const
{
	return Vec2(x - rhs.x,y - rhs.y);
}

Vec2& Vec2::operator-=(const Vec2& rhs)
{
	return *this = *this - rhs;
}

Vec2 Vec2::operator*(const float rhs) const
{
	return Vec2(x * rhs,y * rhs);
}

Vec2& Vec2::operator*=(const float rhs)
{
	return *this = *this * rhs;
}

Vec2 Vec2::operator/(const float rhs) const
{
	return Vec2(x/rhs, y/rhs);
}

Vec2& Vec2::operator/=(const float rhs)
{
	return *this = *this / rhs;
}

float Vec2::GetLength_sq() const
{
	return x * x + y * y;
}

float Vec2::GetLength() const
{
	return std::sqrt(GetLength_sq());
}

Vec2 Vec2::GetNormalized() const
{
	const float len = GetLength();
	if (len == 0)
	{
		return Vec2(0, 0);
	}

	return *this * 1/len;
}

Vec2& Vec2::Normalize()
{
	return *this = GetNormalized();
}
