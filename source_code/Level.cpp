/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "Level.h"

#include "lvl0.h"
#include "lvl1.h"
#include "lvl2.h"
#include "lvl3.h"
#include "lvl4.h"

Level::Level(int curLevel_in)
{
	nLevel = curLevel_in;
	int levelIndex = 0;

	

	for (int y = 0; y < levelDown; ++y)
	{
		for (int x = 0; x < levelAcross; ++x)
		{
			if (nLevel == 0 && lvl0[levelIndex] > 0)
			{
				tiles[levelIndex] = Tile(RectF(Vec2(x * Tile::tileSize, y * Tile::tileSize), Tile::tileSize, Tile::tileSize), lvl0[levelIndex]);
			}
			else if (nLevel == 1 && lvl1[levelIndex] > 0)
			{
				tiles[levelIndex] = Tile(RectF(Vec2(x * Tile::tileSize, y * Tile::tileSize), Tile::tileSize, Tile::tileSize), lvl1[levelIndex]);
			}
			else if (nLevel == 2 && lvl2[levelIndex] > 0)
			{
				tiles[levelIndex] = Tile(RectF(Vec2(x * Tile::tileSize, y * Tile::tileSize), Tile::tileSize, Tile::tileSize), lvl2[levelIndex]);
			}
			else if (nLevel == 3 && lvl3[levelIndex] > 0)
			{
				tiles[levelIndex] = Tile(RectF(Vec2(x * Tile::tileSize, y * Tile::tileSize), Tile::tileSize, Tile::tileSize), lvl3[levelIndex]);
			}
			else if (nLevel == 4 && lvl4[levelIndex] > 0)
			{
				tiles[levelIndex] = Tile(RectF(Vec2(x * Tile::tileSize, y * Tile::tileSize), Tile::tileSize, Tile::tileSize), lvl4[levelIndex]);
			}
			++levelIndex;
		}
	}


	if (nLevel == 1)
	{
		levelTrigger = level1Trigger;
//		lvlDangerTime = lvl1DangerTime;
		lvlTriggerTimer = lvl1DangerTime;
	}	
}

bool Level::Reset()
{
	lvlTriggerTimer = lvlDangerTime;
	levelInDanger = false;
	return false;
}


RectF Level::GetTileRect(int nTile) const
{
	return tiles[nTile].rect;
}

int Level::GetTileType(int nTile) const
{
	if		(nLevel == 0)
		return lvl0[nTile];
	else if (nLevel == 1)
		return lvl1[nTile];
	else if (nLevel == 2)
		return lvl2[nTile];
	else if (nLevel == 3)
		return lvl3[nTile];
	else if (nLevel == 4)
		return lvl4[nTile];

	return 0;
}


void Level::PlaceTile(const Vec2I& gridPos)
{
	if (nLevel == 0)
		lvl0[gridPos.y * levelAcross + gridPos.x] = 1;
	if (nLevel == 1)
		lvl1[gridPos.y * levelAcross + gridPos.x] = 1;
	if (nLevel == 2)
		lvl2[gridPos.y * levelAcross + gridPos.x] = 1;
	if (nLevel == 3)
		lvl3[gridPos.y * levelAcross + gridPos.x] = 1;
	if (nLevel == 4)
		lvl4[gridPos.y * levelAcross + gridPos.x] = 1;

	tiles[gridPos.y * levelAcross + gridPos.x] = Tile(RectF(Vec2(gridPos.x * Tile::tileSize, gridPos.y * Tile::tileSize), Tile::tileSize, Tile::tileSize), 1);
}

void Level::RemoveTile(const Vec2I& gridPos)
{
	if (nLevel == 0)
		lvl0[gridPos.y * levelAcross + gridPos.x] = 0;
	if (nLevel == 1)
		lvl1[gridPos.y * levelAcross + gridPos.x] = 0;
	if (nLevel == 2)
		lvl2[gridPos.y * levelAcross + gridPos.x] = 0;
	if (nLevel == 3)
		lvl3[gridPos.y * levelAcross + gridPos.x] = 0;
	if (nLevel == 4)
		lvl4[gridPos.y * levelAcross + gridPos.x] = 0;

	tiles[gridPos.y * levelAcross + gridPos.x] = Tile(RectF(Vec2(gridPos.x * Tile::tileSize, gridPos.y * Tile::tileSize), Tile::tileSize, Tile::tileSize), 0);
}

bool Level::DoPlayerCollision(Player& player)
{
	RectF pRect = player.GetRect();
	int tileColIndex[4] = {
		int(pRect.top / Tile::tileSize) * AllLevels::levelAcross + int(pRect.left / Tile::tileSize),
		int(pRect.bottom / Tile::tileSize) * AllLevels::levelAcross + int(pRect.right / Tile::tileSize),
		int(pRect.top / Tile::tileSize) * AllLevels::levelAcross + int(pRect.right / Tile::tileSize),
		int(pRect.bottom / Tile::tileSize) * AllLevels::levelAcross + int(pRect.left / Tile::tileSize)
	};

	bool collided = false;
	
	for (int i = 0; i < 4; ++i)
	{
		if (tiles[tileColIndex[i]].CheckPlayerCollision(player) && tiles[tileColIndex[i]].ExecutePlayerCollision(player) != Tile::TileCollisionTypes::NoTileColTypes)
		{
			collided = true;
		}
	}

	return collided;
}

bool Level::Update(float fElapsedTime)
{
	bool hadDangerLoop = lvlDangerLoop;
	if (IsLevelInDanger())
	{
		lvlTriggerTimer -= fElapsedTime;
		if (lvlTriggerTimer < lvl1DangerTime - 6.0f && !lvlDangerLoop)
		{
			lvlDangerLoop = true;
		}
	}
	return lvlDangerLoop && !hadDangerLoop;
}

bool Level::DoEnemyCollision(Enemy0& enemy)
{
	RectF eRect = enemy.GetRect();
	int tileColIndex[4] = {
		int(eRect.top / Tile::tileSize) * levelAcross + int(eRect.left / Tile::tileSize),
		int(eRect.bottom / Tile::tileSize) * levelAcross + int(eRect.right / Tile::tileSize),
		int(eRect.top / Tile::tileSize) * levelAcross + int(eRect.right / Tile::tileSize),
		int(eRect.bottom / Tile::tileSize) * levelAcross + int(eRect.left / Tile::tileSize)
	};

	bool collided = false;

	for (int i = 0; i < 4; ++i)
	{
		if (tiles[tileColIndex[i]].DoEnemyCollision(enemy) != Tile::TileCollisionTypes::NoTileColTypes)
		{
			collided = true;
		}
	}

	return collided;
}




bool Level::IsLevelInDanger() const
{
	return levelInDanger;
}

bool Level::DoPlayerTrigger(Player& player)
{
	bool overlapped = levelTrigger.IsOverLappingWith(player.GetRect()) && !levelInDanger; 

	if (overlapped)
	{
		levelInDanger = true;
	}

	return overlapped;
}

