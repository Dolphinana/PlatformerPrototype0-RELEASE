/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once
#include "RectF.h"
#include "Player.h"
#include "Enemy.h"
#include "Vec2.h"


class Tile
{
public:
	enum class TileCollisionTypes
	{
		NoTileColTypes,
		Side,
		Top,
		Bottom
	};
public:
	Tile() = default;
	Tile(RectF rect_in, int type_in);


	bool CheckPlayerCollision(Player& player) const;
	TileCollisionTypes ExecutePlayerCollision(Player& player);
	TileCollisionTypes DoEnemyCollision(Enemy0& enemy);

	Vec2 GetCenter() const;


	RectF rect = RectF(0,0,0,0);
	int type = 0;
	static constexpr float tileSize = 16.0f;

};
