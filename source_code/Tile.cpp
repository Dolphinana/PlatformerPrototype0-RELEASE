/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "Tile.h"
#include <assert.h>

Tile::Tile(RectF rect_in, int type_in)
	:
	rect(rect_in),
	type(type_in)
{
}



bool Tile::CheckPlayerCollision(Player& player) const
{
	return rect.IsOverLappingWith(player.GetRect());
}


Tile::TileCollisionTypes Tile::ExecutePlayerCollision(Player& player)
{
	if (type == 0)
		return TileCollisionTypes::NoTileColTypes;

	RectF playerrect = player.GetRect();
	RectF playeroldrect = player.GetOldRect();

	assert( CheckPlayerCollision(player) );
	
	if (playeroldrect.bottom <= rect.top && playerrect.bottom >= rect.top)
	{
		player.pos.y = rect.top - player.size.y / 2;
		player.vel.y = 0.0f;
		player.on_floor = true;
		player.on_floor_at_all = true;
		return TileCollisionTypes::Top;
	}
	if (playeroldrect.right <= rect.left && playerrect.right >= rect.left)
	{
		player.pos.x = rect.left - player.size.x / 2;
		player.vel.x = 0.0f;

		return TileCollisionTypes::Side;
	}
	if (playeroldrect.left >= rect.right && playerrect.left <= rect.right)
	{
		player.pos.x = rect.right + player.size.x / 2;
		player.vel.x = 0.0f;

		return TileCollisionTypes::Side;
	}
	if (playeroldrect.top >= rect.bottom && playerrect.top <= rect.bottom)
	{
		player.JumpRelease();
		player.pos.y = rect.bottom + player.size.y / 2;
		player.vel.y = 0.0f;
		return TileCollisionTypes::Bottom;
	}

	
	else player.on_floor = false && !player.on_floor_at_all;


	return TileCollisionTypes::NoTileColTypes;
}



Tile::TileCollisionTypes Tile::DoEnemyCollision(Enemy0& enemy)
{
	if (type == 0)
		return TileCollisionTypes::NoTileColTypes;

	RectF enemyoldrect = enemy.GetOldRect();
	RectF enemyrect = enemy.GetRect();

	const Vec2 oldVelo = enemy.vel;
	assert(enemy.alive);
	if (rect.IsOverLappingWith(enemyrect))
	{
		if (enemyoldrect.bottom <= rect.top && enemyrect.bottom >= rect.top)
		{
			enemy.pos.y = rect.top - enemy.size.y;
			switch (enemy.enemytype)
			{
			case Enemy0::EnemyTypes::WalkyFroggy:
				enemy.vel.y = 0.0f;
				break;
			case Enemy0::EnemyTypes::QuickBird:
				enemy.vel.y = -enemy.vel.y;
				break;
			}
			//assert(enemy.vel.x != oldVelo.x);

			enemy.on_floor = true;
			enemy.on_floor_at_all = true;
			return TileCollisionTypes::Top;
		}
		else if (enemyoldrect.top >= rect.bottom && enemyrect.top <= rect.bottom)
		{
			enemy.pos.y = rect.bottom;
			switch (enemy.enemytype)
			{
			case Enemy0::EnemyTypes::WalkyFroggy:
				enemy.vel.y = 0.0f;

				break;
			case Enemy0::EnemyTypes::QuickBird:
				enemy.vel.y = -enemy.vel.y;
				break;
			}
			//assert(enemy.vel.x != oldVelo.x);
			return TileCollisionTypes::Bottom;
		}
		else if (enemyoldrect.right <= rect.left && enemyrect.right >= rect.left)
		{
			enemy.pos.x = rect.left - enemy.size.x;
			enemy.vel.x = -enemy.vel.x;

			//assert(enemy.vel.y != oldVelo.y);
			return TileCollisionTypes::Side;
		}
		else if (enemyoldrect.left >= rect.right && enemyrect.left <= rect.right)
		{
			enemy.pos.x = rect.right;
			enemy.vel.x = -enemy.vel.x;

			//assert(enemy.vel.y != oldVelo.y);

			return TileCollisionTypes::Side;
		}

	}
	else enemy.on_floor = false && !enemy.on_floor_at_all;

	return TileCollisionTypes::NoTileColTypes;
}




Vec2 Tile::GetCenter() const
{
	return rect.GetCenter();
}
