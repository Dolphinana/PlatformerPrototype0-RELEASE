/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once
#include "Vec2.h"
#include "RectF.h"





/*
             /
//////////// --
//  /        \
//
//

*/




class Gun
{
public:
	enum class GunTypes
	{
		Ordinary,
		Directioner
	};
public:
	Gun() = default;
	Gun(int damage_in);
	
	class Bullet
	{
	public:
		Bullet() = default;


		void Reset();

		RectF GetRect()
		{
			return RectF::FromCenter(pos, size.x / 2, size.y / 2);
		}


		Vec2 vel = Vec2(0,0);
		Vec2 pos = Vec2(0,0);
		bool shoot = false;
		bool initialized = true;
		float speed = 300.f;
		float shootTime = 0;
		Vec2 size = Vec2(10.0f, 10.0f);

	};


	void Update(float fElapsedTime);
	bool DoShoot(const class Player& player);
	bool DoEnemyDamage(class Enemy0& enemy);



	static constexpr float maxShootTime = 0.25f;
	static constexpr int nMaxBullets = 200;
	static constexpr int nBullets = 0;
	Bullet bullet[nMaxBullets];
	int damage = 1;
	bool Equipped = true;
	GunTypes gunType = GunTypes::Ordinary;

	};
