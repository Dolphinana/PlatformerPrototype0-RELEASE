/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once
#include "Tile.h"
#include "Vec2.h"
#include "Player.h"
#include "Vec2I.h"


class Level
{
public:
	Level(int curLevel_in);

	bool Reset();

	static constexpr int levelAcross = 200;
	static constexpr int levelDown = 40;
	static constexpr int maxTiles = levelAcross * levelDown;
	int nLevel;
	Tile tiles[maxTiles];

	bool Update(float fElapsedTime);
	RectF GetTileRect(int nTile) const;
	int GetTileType(int nTile) const;

	void PlaceTile(const Vec2I& gridPos);
	void RemoveTile(const Vec2I& gridPos);

	bool DoPlayerCollision(Player& player);
	bool DoEnemyCollision(Enemy0& enemy);

	bool DoPlayerTrigger(Player& player);

	bool IsLevelInDanger() const;

	bool levelInDanger = false;

	RectF levelTrigger = RectF(0,0,0,0);
	const float lvlDangerTime = 120.0f;
	float lvlTriggerTimer;
	bool lvlDangerLoop = false;
	bool lvlDestroyed = false;
};
