/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once
#include "Vec2.h"
#include "RectF.h"
#include "Gun.h"
#include "Particle.h"


class Player
{
public:
	enum class LookingType
	{
		Straight,
		Up,
		Down
	};
public:
	Player() = default;
	Player(const Vec2& pos_in);

	void Init(const Vec2& pos_in);
	void Update(float fElapsedTime);

	void JumpStart();
	void JumpHold();
	void JumpRelease();


	RectF GetRect() const
	{
		return RectF::FromCenter(pos, size.x / 2, size.y / 2);
		//return RectF::FromCenter(pos - Vec2(0.f,1.f), size.x / 2, size.y / 2);
	}
	RectF GetOldRect() const
	{
		return RectF::FromCenter(oldpos, size.x / 2, size.y / 2);
		//return RectF::FromCenter(oldpos - Vec2(0.f,1.f), size.x / 2, size.y / 2);
	}

	bool GetGameover() const;
	void SetGameover(bool gameOver_in);

	bool on_floor_at_all = false;
	bool was_on_floor = false;
	bool on_floor = false;
	const Vec2 size = Vec2(10.0f, 14.0f);
	const Vec2 spriteSize = Vec2(16.0f,16.0f);
	const float acc = 300.f;
	const float speed = 120.0f;
	const float jumpPower = 210.0f;
	float jumpTime = 0.f;
	bool turnedRight = true;
	bool walking = false;
	bool walkingToRight = false;
	float walkTime = 0.0f;
	int spriteFrame = 0;
	LookingType looking = LookingType::Straight;

	Vec2 pos;
	Vec2 oldpos;
	Vec2 vel;

	Particle deathParticles;

	Gun gun = Gun(1);

private:
	bool gameOver = false;

};

