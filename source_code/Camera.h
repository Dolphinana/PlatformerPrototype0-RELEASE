/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once
#include "Player.h"

class Camera
{
public:
	void Update(const Player& player, const Vec2& screenSize, float fElapsedTime, bool shaking = false)
	{
		time += fElapsedTime;


		if (shaking)
		{
			// ooffset = sin/cos (time * speed) * how_far_should_offset_be?
			shakeOffset.x = sin(time * 90) * 20;
			shakeOffset.y = -cos(time * 100) * 30;
		}
		else
		{
			shakeOffset = Vec2(0.f, 0.f);
		}



		if (player.turnedRight)
		{
			offset.x += cameraOffsetSpeed * fElapsedTime;
			if (offset.x >= cameraMaxOffset)
			{
				offset.x = cameraMaxOffset;
			}
		}
		else if (!player.turnedRight)
		{
			offset.x -= cameraOffsetSpeed * fElapsedTime;
			if (offset.x <= -cameraMaxOffset)
			{
				offset.x = -cameraMaxOffset;
			}
		}

		switch (player.looking)
		{
		case Player::LookingType::Up:
			offset.y -= cameraOffsetSpeed * fElapsedTime;
			if (offset.y <= -cameraMaxOffset)
			{
				offset.y = -cameraMaxOffset;
			}
			break;
		case Player::LookingType::Down:
			offset.y += cameraOffsetSpeed * fElapsedTime;
			if (offset.y >= cameraMaxOffset)
			{
				offset.y = cameraMaxOffset;
			}
			break;
		case Player::LookingType::Straight:
			if (std::signbit(offset.y))
			{
				offset.y += (cameraOffsetSpeed * fElapsedTime) / 1;
			}
			else
			{
				offset.y -= (cameraOffsetSpeed * fElapsedTime) / 1;
			}


		/*	if (signbit(offset.y))
			{
				offset.y -= (cameraOffsetSpeed * fElapsedTime) / 1;
			}
			else
			{
				offset.y += (cameraOffsetSpeed * fElapsedTime) / 1;
			}

			if (offset.y < 2.f && offset.y > -2.f)
			{
				offset.y = 0.0f;
			}*/
			break;
		}

		

		// Move camera to player
		pos.x += ((shakeOffset.x + offset.x + player.pos.x - pos.x - screenSize.x / 2) * 6) * fElapsedTime;
		pos.y += ((shakeOffset.y + offset.y + player.pos.y - pos.y - screenSize.y / 2) * 6) * fElapsedTime;
	}

	Vec2 pos;
	Vec2 offset;
	Vec2 shakeOffset = { 0.f,0.f };
	const float cameraOffsetSpeed = 120.0f;
	const float cameraMaxOffset = 60.0f;
	float time = 0.0f;
};
