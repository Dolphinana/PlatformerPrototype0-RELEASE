/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once
#include "Level.h"
#include "Enemy.h"
#include "Vec2.h"
#include "Player.h"
#include "Vec2I.h"


class AllLevels
{
public:

	bool Reset();
	void OnMouse0Hold(const Vec2I& screenPos, const Vec2I& camPos);
	void OnMouse1Hold(const Vec2I& screenPos, const Vec2I& camPos);

	bool Update(float fElapsedTime);
	Vec2 GetPlayerSpawnPoint() const;
	float GetPlayerSpawnPointX() const;
	float GetPlayerSpawnPointY() const;
	Vec2 GetEnemySpawnPoint(int nEnemy) const;
	Enemy0::EnemyTypes GetIndividualEnemyTypes(int nEnemy) const;
	RectF GetTileRect(int nTile) const;
	int GetTileType(int nTile) const;
	bool DoPlayerCollision(Player& player);
	bool DoEnemyCollision(Enemy0& enemy);
	bool DoPlayerTrigger(Player& player);

	bool IsLevelInDanger() const;
		

	Vec2I ScreenToGrid(const Vec2I& screenPos) const;

	// tiles per level
	static constexpr int levelAcross = 200;
	static constexpr int levelDown = 40;
	static constexpr int maxTiles = levelAcross * levelDown;


	int curLevel = 0;
private:
	static constexpr int nLevels = 5;
	const Vec2 enemySpawnPoint[nLevels][20/*nEnemy0*/] = {
		{
			Vec2(400,  300),
			Vec2(1400, 100),
			Vec2(1900, 100),
			Vec2(1910, 100),
			Vec2(1920, 100),
			Vec2(1930, 100),
			Vec2(1940, 100),
			Vec2(1950, 100),
			Vec2(1960, 100),
			Vec2(1970, 100),
			Vec2(1980, 100),
			Vec2(1990, 100),
			Vec2(2000, 100),
			Vec2(2010, 100),
			Vec2(2020, 100),
			Vec2(2030, 100),
			Vec2(2040, 100),
			Vec2(2050, 100),
			Vec2(2060, 100),
			Vec2(2070, 100)
		},

		{
			Vec2(690,   265),
			Vec2(850 ,  500),
			Vec2(999 ,  520),
			Vec2(1910,   0),
			Vec2(1920,   0),
			Vec2(1930,   0),
			Vec2(1940,   0),
			Vec2(1950,   0),
			Vec2(1960,   0),
			Vec2(1970,   0),
			Vec2(1980,   0),
			Vec2(1990,   0),
			Vec2(2000,   0),
			Vec2(2010,   0),
			Vec2(2020,   0),
			Vec2(2030,   0),
			Vec2(2040,   0),
			Vec2(2050,   0),
			Vec2(2060,   0),
			Vec2(2070,   0)
		},

		{
			Vec2(400,  0),
			Vec2(1400, 0),
			Vec2(1900, 0),
			Vec2(1910, 0),
			Vec2(1920, 0),
			Vec2(1930, 0),
			Vec2(1940, 0),
			Vec2(1950, 0),
			Vec2(1960, 0),
			Vec2(1970, 0),
			Vec2(1980, 0),
			Vec2(1990, 0),
			Vec2(2000, 0),
			Vec2(2010, 0),
			Vec2(2020, 0),
			Vec2(2030, 0),
			Vec2(2040, 0),
			Vec2(2050, 0),
			Vec2(2060, 0),
			Vec2(2070, 0)
		},
		{
			Vec2(400,  0),
			Vec2(1400, 0),
			Vec2(1900, 0),
			Vec2(1910, 0),
			Vec2(1920, 0),
			Vec2(1930, 0),
			Vec2(1940, 0),
			Vec2(1950, 0),
			Vec2(1960, 0),
			Vec2(1970, 0),
			Vec2(1980, 0),
			Vec2(1990, 0),
			Vec2(2000, 0),
			Vec2(2010, 0),
			Vec2(2020, 0),
			Vec2(2030, 0),
			Vec2(2040, 0),
			Vec2(2050, 0),
			Vec2(2060, 0),
			Vec2(2070, 0)
		},

		{

			Vec2(400,  0),
			Vec2(1400, 0),
			Vec2(1900, 0),
			Vec2(1910, 0),
			Vec2(1920, 0),
			Vec2(1930, 0),
			Vec2(1940, 0),
			Vec2(1950, 0),
			Vec2(1960, 0),
			Vec2(1970, 0),
			Vec2(1980, 0),
			Vec2(1990, 0),
			Vec2(2000, 0),
			Vec2(2010, 0),
			Vec2(2020, 0),
			Vec2(2030, 0),
			Vec2(2040, 0),
			Vec2(2050, 0),
			Vec2(2060, 0),
			Vec2(2070, 0)
		}
	};
	const Enemy0::EnemyTypes individualEnemyTypes[nLevels][20/*nEnemy0*/] = { 
		// Level 0
		{
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird
		},

		// Level 1
		{
			Enemy0::EnemyTypes::WalkyFroggy,
			Enemy0::EnemyTypes::WalkyFroggy,
			Enemy0::EnemyTypes::WalkyFroggy,
			Enemy0::EnemyTypes::WalkyFroggy,
			Enemy0::EnemyTypes::WalkyFroggy,
			Enemy0::EnemyTypes::WalkyFroggy,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes,
			Enemy0::EnemyTypes::NoEnemyTypes
		},

		// Level 2
		{
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird
		},

		{
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird
		},
	
		{
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird,
			Enemy0::EnemyTypes::QuickBird
		}
	};

/*
	Vec2 playerSpawnPoint[nLevels] = {
		Vec2(40,40),				// Level 0
		Vec2(1750,360),				// Level 1
		Vec2(30.0f, 120.0f),		// Level 2
		Vec2(1505.0f, 200.0f),		// Level 3
		Vec2(30.0f, 120.0f),		// Level 4
	};
	*/


	Level levels[nLevels] = {0,1,2,3,4};
};
