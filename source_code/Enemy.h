/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once
#include "Vec2.h"
#include "RectF.h"
#include "Player.h"

class Enemy0
{
public:
	enum class EnemyTypes
	{
		NoEnemyTypes,
		WalkyFroggy,
		QuickBird
	};

	enum class PlayerCollideTypes
	{
		NoColTypes,
		Overlapping,
		Stomped
	};
public:
	Enemy0() = default;
	Enemy0( const Vec2& pos_in, EnemyTypes type_in );

	void Init(const Vec2& pos_in, EnemyTypes type_in);
	void Update(float fElapsedTime);
	PlayerCollideTypes CollidePlayer(Player& player);
	void TakeDamage(int damage_in);
	void FollowPlayer(const Player& player, float fElapsedTime);


	RectF GetRect() const
	{
		return RectF(pos, size.x, size.y);
	}
	RectF GetOldRect() const
	{
		return RectF(oldpos, size.x, size.y);
	}


	const float fallAcc = 600.f;


	Vec2 pos;// = Vec2(0.f, 0.f);
	Vec2 oldpos = Vec2(0.f, 0.f);
	Vec2 vel = Vec2(0.f, 0.f);
	Vec2 size = Vec2(16.0f, 12.0f);
	bool on_floor_at_all = false;
	bool on_floor = false;
	bool initialized = false;
	bool alive = false;
	int hp = 0;
	static constexpr float flySpeedCap = 250.0f;

	EnemyTypes enemytype = EnemyTypes::WalkyFroggy;
};
