/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include "AllLevels.h"




bool AllLevels::Reset()
{
	levels[curLevel].Reset();
	return true;
}

void AllLevels::OnMouse0Hold(const Vec2I& screenPos, const Vec2I& camPos)
{
	levels[curLevel].PlaceTile(ScreenToGrid(screenPos + camPos));
}

void AllLevels::OnMouse1Hold(const Vec2I& screenPos, const Vec2I& camPos)
{
	levels[curLevel].RemoveTile((screenPos + camPos) / Tile::tileSize);
}

bool AllLevels::Update(float fElapsedTime)
{
	return (levels[curLevel].Update(fElapsedTime));
}

Vec2 AllLevels::GetPlayerSpawnPoint() const
{
	//return playerSpawnPoint[curLevel];
	if (curLevel == 0)
	{
		return {40,40};	
	}
	else if (curLevel == 1)
	{
		return Vec2(1750,360);	
	}
	else if (curLevel == 2)
	{
		return Vec2(30.0f, 120.0f);
	}
	else if (curLevel == 3)
	{
		return Vec2(1505.0f, 200.0f);
	}
	else
	{
		Vec2(30.0f, 120.0f);
	}
}

float AllLevels::GetPlayerSpawnPointY() const
{
	//return playerSpawnPoint[curLevel];
	if (curLevel == 0)
	{
		return 40;	
	}
	else if (curLevel == 1)
	{
		return 360;	
	}
	else if (curLevel == 2)
	{
		return 120.0f;
	}
	else if (curLevel == 3)
	{
		return 200.0f;
	}
	else
	{
		return 120.0f;
	}
}

float AllLevels::GetPlayerSpawnPointX() const
{
	//return playerSpawnPoint[curLevel];
	if (curLevel == 0)
	{
		return 40;	
	}
	else if (curLevel == 1)
	{
		return 1750;	
	}
	else if (curLevel == 2)
	{
		return 30.0f;
	}
	else if (curLevel == 3)
	{
		return 1505.0f;
	}
	else
	{
		return 30.0f;	
	}
}

Vec2 AllLevels::GetEnemySpawnPoint(int nEnemy) const
{
	return enemySpawnPoint[curLevel][nEnemy];
}

Enemy0::EnemyTypes AllLevels::GetIndividualEnemyTypes(int nEnemy) const
{
	return individualEnemyTypes[curLevel][nEnemy];
}

RectF AllLevels::GetTileRect(int nTile) const
{
	return levels[curLevel].GetTileRect(nTile);
}

int AllLevels::GetTileType(int nTile) const
{
	return levels[curLevel].GetTileType(nTile);
}

bool AllLevels::DoPlayerCollision(Player& player)
{
	return levels[curLevel].DoPlayerCollision(player);
}

bool AllLevels::DoEnemyCollision(Enemy0& enemy)
{
	return levels[curLevel].DoEnemyCollision(enemy);
}

bool AllLevels::DoPlayerTrigger(Player& player)
{
	return levels[curLevel].DoPlayerTrigger(player);
}

bool AllLevels::IsLevelInDanger() const
{
	return levels[curLevel].IsLevelInDanger();
}

Vec2I AllLevels::ScreenToGrid(const Vec2I& screenPos) const
{
	return screenPos/Tile::tileSize;
}
