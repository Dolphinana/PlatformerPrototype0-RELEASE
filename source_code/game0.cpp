/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/*
 2021-08-13: I came back to this project and this time, i'm using g++ on Arch Linux and 
 when i made Makefile and builded the project, i got a shitton of errors!!! 
 Probably because previously i used a MVS 2019 and now i use g++ which is slightly different.
 Hopefully i can fix these errors.
 Annnnndd 23:39:55 i got the game up and running and the game is really broken like player's not there :D
00:03:15 I know why player isn't showing up, it's because enemy spawns at exact same location thus killing 
the player... I should go sleep.

2021-09-16: 20:43:00 The code compiles sound system for the game on GNU/Linux! Let's turn uncomment that sound and...
The sound isn't working... whaat???
21:30:55 I looked around olcPixelGameEngine Github and found pull request for PGEX_Sound and i modified the soundExt like 
that guy did and now GNU+Linux version got sound!!! :D

 */



/*TODO:
Enemies :)
Jump stop :)
Gamescreen should be wider :)
exit the game with escape key :)
Be able to reset :)
Speedcap for flying enemies :)
Make it so enemies move out of the way if they 100% overlapp each other :(
Fix player size :(
Ropes! :(
If (player == running) {jump(higher);} :(
Finish the game :(
 

 

 In the first level, when you get to the end of the level and do something, the place get's really unstable and you need to
 evacuate immediately. If you damage something even more. You'll activate hard mode and time to evacuate is less.

 */
 






#include "game0.h"

//#include <random>
#include <iostream>
#include <assert.h>



//static constexpr int nOldFPS = 100;
//int oldFPS[nOldFPS];
//int oldFPSindex = 0;
//int avgFPS = 0;




enum GameState
{
	Title,
	Playing
};

enum PlayMode
{
	Normal,
	Assisted,
	Building
};

GameState gameState = GameState::Title;
PlayMode playMode = PlayMode::Normal;
int curPlayMode = playMode;
const int nPlayMode = 3;


	void Game0::HardReset_or_Create()
	{
		PlayMode playMode = PlayMode::Normal;

		cam.pos = Vec2(0.0f, 0.0f);
		player.Init( allLevels.GetPlayerSpawnPoint() );



//		allLevels = AllLevels();


		for (int i = 0; i < nEnemy0; ++i)
		{
			enemy0[i].Init(allLevels.GetEnemySpawnPoint(i), allLevels.GetIndividualEnemyTypes(i));
			//enemy0[i].Init(Vec2(100,100), allLevels.GetIndividualEnemyTypes(i));
		}

		spritePlayer = std::make_unique<olc::Sprite>("./data/whiteboi.png");
		spriteTiles0 = std::make_unique<olc::Sprite>("./data/tiles0.png");
		spriteTitle = std::make_unique<olc::Sprite>("./data/press_space.png");
		spriteAlfabet = std::make_unique<olc::Sprite>("./data/alfabet.png");
		spritePlayMode = std::make_unique<olc::Sprite>("./data/playmode.png");

		for (Particle& p : particles)
		{
			p = Particle(Vec2(float(rand() % 200), float(rand() % 200)), Vec2(float(rand() % 200), float(rand() % 200)), true);
		}


		//player.SetGameover(false);
		//player.pos = playerSpawnPoint[curLevel];


		//// Init function that calls constructor dosen't work for some reason...
		//// so i just do data = data_in
		//for (int i = 0; i < nEnemy0; ++i)
		//{
		//	enemy0[i].Init(enemySpawnPoint[curLevel][i], individualEnemyTypes[curLevel][i]);
		//}


		if (audioInitialized)
			olc::SOUND::DestroyAudio();

		olc::SOUND::InitialiseAudio(44100,1,8,512);

		audioInitialized = true;
		

/*
		level1InDanger = false;
		lvl1TriggerTimer = lvl1DangerTime;
		lvl1DangerLoop = false;
		lvl1Destroyed = false;
*/

		allLevels.Reset();
	}

	bool Game0::OnUserCreate()
	{
		// Create cool things here!

		
		sounds.shoot = olc::SOUND::LoadAudioSample("./data/shoot0.wav");
		sounds.playerjump = olc::SOUND::LoadAudioSample("./data/noise1.wav");
		sounds.enemydamage = olc::SOUND::LoadAudioSample("./data/hurtDistorted0.wav");
		sounds.playerdamage = olc::SOUND::LoadAudioSample("./data/shoot0.wav");
		sounds.explosion0 = olc::SOUND::LoadAudioSample("./data/explosion0.wav");
		sounds.intro_lvl1escape = olc::SOUND::LoadAudioSample("./data/intro_lvl1escapeMONO.wav");
		sounds.loop_lvl1escape = olc::SOUND::LoadAudioSample("./data/loop_lvl1escapeMONO.wav");
		

		HardReset_or_Create();

		return true;
	}


	void Game0::SoftReset()
	{
		player.SetGameover(false);
		player.pos = allLevels.GetPlayerSpawnPoint();
	}






	bool Game0::OnUserUpdate(float fElapsedTime)
	{
		// Called once per frame so update your stuff here

		/*oldFPS[oldFPSindex] = GetFPS();
		oldFPSindex++;
		if (oldFPSindex >= nOldFPS)
		{
			if (oldFPSindex > nOldFPS)
			{
				std::wcout << "WARNING: oldFPSindex >= nOldFPS" << std::endl;
			}
			oldFPSindex = 0;


			int result = 0;
			for (int i = 0; i < nOldFPS; ++i)
			{
				result += oldFPS[i];
			}
			result /= nOldFPS;

			for (int i = 0; i < 100; ++i)
			{
				std::cout << "AVGFPS: " << result << std::endl;
			}
		}*/


		Clear(olc::VERY_DARK_BLUE);

		if (allLevels.Update(fElapsedTime))
		{
			olc::SOUND::PlaySample(sounds.loop_lvl1escape, true);
		}


		if (GetKey(olc::Key::ESCAPE).bPressed)
		{
			++escapePress;
			timeUntilCancel = 0.0f;
			if (escapePress >= 2)
			{
				assert(escapePress == 2);
				return false;
			}
		}
		else
		{
			timeUntilCancel += fElapsedTime;
			if (timeUntilCancel > 5.0f)
			{
				escapePress = 0;
			}
		}


		if (GetKey(olc::Key::TAB).bPressed)
		{
			curPlayMode++;
			if (curPlayMode >= nPlayMode)
			{
				curPlayMode = 0;
			}

			switch (curPlayMode)
			{
			case 0:
				playMode = PlayMode::Normal;
				break;
			case 1:
				playMode = PlayMode::Building;
				break;
			case 2:
				playMode = PlayMode::Assisted;
				break;
			}
		}


		switch (gameState)
		{

		case GameState::Title:
			if (GetKey(olc::Key::SPACE).bPressed)
			{
				gameState = GameState::Playing;
			}
			break;

		case GameState::Playing:
			if (GetKey(olc::Key::K0).bPressed)
			{
				allLevels.curLevel = 0;
				HardReset_or_Create();
			}
			if (GetKey(olc::Key::K1).bPressed)
			{
				allLevels.curLevel = 1;
				HardReset_or_Create();
			}
			if (GetKey(olc::Key::K2).bPressed)
			{
				allLevels.curLevel = 2;
				HardReset_or_Create();
			}
			if (GetKey(olc::Key::K3).bPressed)
			{
				allLevels.curLevel = 3;
				HardReset_or_Create();
			}
			if (GetKey(olc::Key::K4).bPressed)
			{
				allLevels.curLevel = 4;
				HardReset_or_Create();
			}


			if (GetKey(olc::Key::T).bPressed)
			{
				SoftReset();
			}
			if (GetKey(olc::Key::R).bPressed)
			{
				HardReset_or_Create();
			}




			// LOGICS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			for (Enemy0& e0 : enemy0)
			{
				e0.FollowPlayer(player, fElapsedTime);
				e0.Update(fElapsedTime);
			}





			// 2021-07-02: Finally... I made it so you can't hold Z to jump everytime you land on floor, you have to press it. It probably took me hours to fix it :)


			if (!player.GetGameover())
			{
				bool just_landed_on_floor = false;
				player.Update(fElapsedTime);
				if (player.on_floor)
				{
					if (player.jumpTime > 0.00001f)
					{
						just_landed_on_floor = true;
					}
					player.jumpTime = 10.0f;
				}
				else
				{
					player.jumpTime += 60.f * fElapsedTime;
				}






				if ((GetKey(olc::Key::UP).bHeld || GetKey(olc::Key::W).bHeld))
				{
					player.looking = Player::LookingType::Up;
				}
				else if ((GetKey(olc::Key::DOWN).bHeld || GetKey(olc::Key::S).bHeld))
				{
					player.looking = Player::LookingType::Down;
				}
				else
				{
					player.looking = Player::LookingType::Straight;
				}

				// If jump is pressed
				if ((GetKey(olc::Key::Z).bPressed || GetKey(olc::Key::K).bPressed ||
					GetKey(olc::Key::SPACE).bPressed))
				{
					if (player.on_floor)
					{
						player.JumpStart();
						olc::SOUND::PlaySample(sounds.playerjump);
					}
					isJumpPressed = true;
				}

				// If jump is held down
				if ((GetKey(olc::Key::Z).bHeld || GetKey(olc::Key::K).bHeld ||
					GetKey(olc::Key::SPACE).bHeld))
				{
					if (!player.on_floor && !player.was_on_floor)
					{
						player.JumpHold();
					}
					isJumpPressed = true;
				}
				// If jump NOT pressed
				else if (!(GetKey(olc::Key::Z).bHeld || GetKey(olc::Key::K).bHeld ||
					GetKey(olc::Key::SPACE).bHeld))
				{
					if (!player.on_floor)
					{
						player.JumpRelease();
					}
					isJumpPressed = false;
				}




				player.walking = false;
				if ((GetKey(olc::Key::LEFT).bHeld || GetKey(olc::Key::A).bHeld) && player.vel.x > -player.speed)
				{
					player.vel.x -= player.acc * fElapsedTime;
					player.turnedRight = false;
					player.walking = true;
					player.walkingToRight = false;
				}
				else if (player.vel.x < 0.01f && player.on_floor)
				{
					player.vel.x += (player.acc / 1.5f) * fElapsedTime;

					/*if (!player.walkingToRight)
						player.walking = false;*/
				}

				if ((GetKey(olc::Key::RIGHT).bHeld || GetKey(olc::Key::D).bHeld) && player.vel.x < player.speed)
				{
					player.vel.x += player.acc * fElapsedTime;
					player.turnedRight = true;
					player.walking = true;
					player.walkingToRight = true;
				}
				else if (player.vel.x > 0.01f && player.on_floor)
				{
					player.vel.x -= (player.acc / 1.5f) * fElapsedTime;
					player.walkingToRight = false;

				}


				player.vel.y += fallAcc * fElapsedTime;
				if (player.vel.y > gravity) player.vel.y = gravity;
				player.pos += player.vel * fElapsedTime;




				if (GetKey(olc::Key::X).bPressed || GetKey(olc::Key::J).bPressed || GetMouse(2).bPressed)
				{
					player.gun.DoShoot(player);
					olc::SOUND::PlaySample(sounds.shoot);
				}
			}

			player.gun.Update(fElapsedTime);


			cam.Update(player, Vec2(ScWidth, ScHeight), fElapsedTime, allLevels.IsLevelInDanger());


			for (Enemy0& e0 : enemy0)
			{
				if (player.gun.DoEnemyDamage(e0))
				{
					olc::SOUND::PlaySample(sounds.enemydamage);
					olc::SOUND::PlaySample(sounds.explosion0);
				}
			}

			for (Enemy0& e0 : enemy0)
			{
				if (!player.GetGameover())
				{
					switch (e0.CollidePlayer(player))
					{
					case Enemy0::PlayerCollideTypes::Stomped:
						olc::SOUND::PlaySample(sounds.enemydamage);
						olc::SOUND::PlaySample(sounds.explosion0);
						break;
					case Enemy0::PlayerCollideTypes::Overlapping:
						olc::SOUND::PlaySample(sounds.playerdamage);
						olc::SOUND::PlaySample(sounds.explosion0);
						player.SetGameover(true);
						break;
					}
				}
			}



			/*bool collisionHappened = false;
			float colDistance_Sq;
			int TileIndex;*/




			// avg FPS 540 when only checking the tile player or enemy is near at
			// avg FPS 510 when checking every tile for player 
			// 
			// 
			// I noticed there's a huge improvement when running the game in debug mode: before tile optimize 50 fps, after optimize 240 fps :)

				//assert(tileIndex[i] < maxTiles);


			if (allLevels.DoPlayerCollision(player))
			{
			}
			player.on_floor = player.on_floor_at_all;

			if (allLevels.DoPlayerTrigger(player))
			{
					olc::SOUND::PlaySample(sounds.explosion0);
					olc::SOUND::PlaySample(sounds.intro_lvl1escape);
			}

			for (Enemy0& e0 : enemy0)
			{
				if (e0.alive)
				{
					allLevels.DoEnemyCollision(e0);
				}
			}



			for (Particle& p : particles)
			{
				p.Update(fElapsedTime, Vec2(ScWidth, ScHeight));
			}


			if (GetMouse(0).bHeld)
			{
				allLevels.OnMouse0Hold(GetMousePos(), Vec2I( int(cam.pos.x), int(cam.pos.y) ) );
			}
			if (GetMouse(1).bHeld)
			{
				allLevels.OnMouse1Hold(GetMousePos(), Vec2I( int(cam.pos.x), int(cam.pos.y) ) );
			}
		}



		/*if (curLevel == 1)
		{
			if (level1InDanger)
			{
				lvl1TriggerTimer -= fElapsedTime;
				if (lvl1TriggerTimer < lvl1DangerTime - 6.0f && !lvl1DangerLoop)
				{
					lvl1DangerLoop = true;
				}
			}

			if (lvl1TriggerTimer < 0.0f && !lvl1Destroyed)
			{
				lvl1Destroyed = true;
				player.SetGameover(true);
				olc::SOUND::PlaySample(sounds.playerdamage);
				olc::SOUND::PlaySample(sounds.explosion0);
				olc::SOUND::StopSample(sounds.loop_lvl1escape);
			}

			if (level1Trigger.IsOverLappingWith(player.GetRect()))
			{
				if (!level1InDanger)
				{
				}
				level1InDanger = true;
			}
			*/
//		player.pos = Vec2(rand() % 1000,rand() % 1000);
		




		///////////////////////////////////////////////////////////////////
		// Draw your stuff here! :) Rita här!	///////////////////////////
		///////////////////////////////////////////////////////////////////


		SetPixelMode(olc::Pixel::NORMAL);

		switch(gameState)
		{
		case GameState::Title:
			DrawSprite(20, 20, spriteTitle.get());
			break;

		case GameState::Playing:
			for (Particle& p : particles)
			{
				Draw(int(p.pos.x), int(p.pos.y), olc::WHITE);
			}

			// Draw Player
			if (!player.GetGameover())
			{
				switch (player.looking)
				{
				case Player::LookingType::Up:
					player.spriteFrame = 2;
					if (player.walkTime > 3.f || !player.on_floor)
					{
						player.spriteFrame += 1;
						if (player.walkTime > 6.f)
							player.walkTime = 0.f;
					}
					break;
				case Player::LookingType::Down:
					player.spriteFrame = 4;
					if (player.walkTime > 3.f || !player.on_floor)
					{
						player.spriteFrame += 1;
						if (player.walkTime > 6.f)
							player.walkTime = 0.f;
					}
					break;

				}

				if (abs(player.vel.x) < 20.f && player.on_floor)
				{
					player.spriteFrame = 0;
					switch (player.looking)
					{
					case Player::LookingType::Up:
						player.spriteFrame = 2;
						break;
					case Player::LookingType::Down:
						player.spriteFrame = 4;
						break;
					}

				}

				//DrawRect( int(player.pos.x - cam.pos.x), int(player.pos.y - cam.pos.y), int(player.pos.x - cam.pos.x- player.spriteSize.x), int(player.pos.y - cam.pos.y - player.spriteSize.y), olc::GREEN);
				//DrawSprite(int(player.pos.x - cam.pos.x - player.spriteSize.x / 2), int(player.pos.y - cam.pos.y - player.spriteSize.y / 2), spritePlayer.get() );
				SetPixelMode(olc::Pixel::MASK);

				DrawPartialSprite(int(player.pos.x - cam.pos.x - player.spriteSize.x / 2), int( (player.pos.y - cam.pos.y - player.spriteSize.y / 2) - 1.f ), spritePlayer.get(), int(player.spriteSize.x * player.spriteFrame), 0, int(player.spriteSize.x), int(player.spriteSize.y), 1, !player.turnedRight);
			}
			//DrawSprite(int(player.pos.x - cam.pos.x - player.size/2), int(player.pos.y - cam.pos.y - player.size / 2), spritePlayer.get(), 1, !player.turnedRight);
			//DrawRect(int(pRect.left - cam.pos.x), int(pRect.top - cam.pos.y), int(player.size), int(player.size), olc::Pixel(255,255,255));


			SetPixelMode(olc::Pixel::NORMAL);
			// Draw Enemies
			for (Enemy0& e0 : enemy0)
			{
				if (e0.alive)
					switch (e0.enemytype)
					{
					case Enemy0::EnemyTypes::WalkyFroggy:
						DrawRect(int(e0.GetRect().left - cam.pos.x), int(e0.GetRect().top - cam.pos.y), int(e0.size.x), int(e0.size.y), olc::Pixel(255, 0, 0));
						break;
					case Enemy0::EnemyTypes::QuickBird:
						DrawRect(int(e0.GetRect().left - cam.pos.x), int(e0.GetRect().top - cam.pos.y), int(e0.size.x), int(e0.size.y), olc::Pixel(255, 0, 0));
						break;
					}
			}

			for (int i = 0; i < AllLevels::maxTiles; ++i)
			{
				if (allLevels.GetTileType(i) > 0)
				{
					const RectF rect = allLevels.GetTileRect(i);
					const int tileleft = int(rect.left - cam.pos.x);
					const int tiletop = int(rect.top - cam.pos.y);
					const int tileright = int(rect.right - cam.pos.x);
					const int tilebottom = int(rect.bottom - cam.pos.y);


					// 1200 FPS avg when checking tile before drawing
					// 350 FPS avg without checking

					if (tileright >= 0 &&
						tilebottom >= 0 &&
						tileleft < ScWidth &&
						tiletop < ScHeight
						)
					{
						const int tilewidth = int(rect.right - rect.left);
						const int tileheight = int(rect.bottom - rect.top);
						DrawPartialSprite(tileleft, tiletop, spriteTiles0.get(), allLevels.GetTileType(i) * int(Tile::tileSize), 0, tilewidth, tileheight, 1, false);
					}
				}


			}

			//SetPixelMode(olc::Pixel::NORMAL);

			if (player.gun.bullet[0].shoot)
			{
				const Vec2 bulletPos = player.gun.bullet[0].pos;
				const Vec2 bulletSize = player.gun.bullet[0].size;
				DrawRect(int(bulletPos.x - cam.pos.x - bulletSize.x / 2), int(bulletPos.y - cam.pos.y - bulletSize.y / 2), int(bulletSize.x), int(bulletSize.y), olc::WHITE);


				// Draw bullet center for debugging purposes
				//Draw(int(bulletPos.x - cam.pos.x), int(bulletPos.y - cam.pos.y), olc::RED);
			}


			/*
			std::cout << "Player pos " << player.pos.x << "," << player.pos.y << std::endl;
			std::cout << "Player pos INT " << int(player.pos.x) << "," << int(player.pos.y) << std::endl;
			*/

			// Draw red dot that represents position of player
			//Draw(player.pos.x - cam.pos.x, player.pos.y - cam.pos.y, olc::RED);

			//SetPixelMode(olc::Pixel::NORMAL);

				
			break;
		}
		SetPixelMode(olc::Pixel::MASK);
		DrawPartialSprite(50, 50, spriteAlfabet.get(), int(fontNum) * fontSize, 0, fontSize, fontSize, 1);
		fontNum += fElapsedTime;
		if (fontNum >= 40)
			fontNum = 0;



		SetPixelMode(olc::Pixel::NORMAL);
		if (playMode == PlayMode::Building)
		{
			const int tileSize = int(Tile::tileSize);
			Vec2I uiRect0 = Vec2I(0, ScHeight - 50);
			Vec2I uiRect1 = Vec2I(ScWidth - 50, 50);
			// I want this thing to be transparent
			FillRect(uiRect0.x, uiRect0.y, uiRect1.x, uiRect1.y, olc::WHITE);

		//	const int tilewidth = int(rect.right - rect.left);
		//	const int tileheight = int(rect.bottom - rect.top);
			for (int x = uiRect0.x; x < uiRect1.x/tileSize; ++x)
			{
				DrawPartialSprite( (tileSize/2) + (x * tileSize), (tileSize/2) + uiRect0.y, spriteTiles0.get(), (x + 1) * int(Tile::tileSize), 0, tileSize, tileSize, 1, false);
			}

		}


		// Draw PlayMode
		DrawPartialSprite(ScWidth - playModeSprWidth, ScHeight - Game0::playModeSprHeight, spritePlayMode.get(), 0, curPlayMode* playModeSprHeight, playModeSprWidth, playModeSprHeight);



		if (escapePress > 0)
		{
			DrawRect(20, 20, int(sin(player.pos.x / 20) * 100), int(sin(player.pos.y / 20) * 100), olc::RED);
		}


		return true;
	}








bool Game0::OnUserDestroy()
{
	olc::SOUND::DestroyAudio();
	return true;
}

int main()
{
	Game0 game;
	if (game.Construct(ScWidth, ScHeight, 3, 3))
		game.Start();

	return 0;
}
