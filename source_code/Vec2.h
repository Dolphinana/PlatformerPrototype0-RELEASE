/*
    PlatformerPrototype0 - I made a decent platformer ;)
    Copyright (C) 2022  Dolphinana

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#pragma once 

class Vec2
{
public:
	Vec2() = default;
	Vec2(float x_in, float y_in);

	Vec2 operator+(const Vec2& rhs) const;
	Vec2& operator+=(const Vec2& rhs);

	Vec2 operator-(const Vec2& rhs) const;
	Vec2& operator-=(const Vec2& rhs);

	Vec2 operator*(const float rhs) const;
	Vec2& operator*=(const float rhs);

	Vec2 operator/(const float rhs) const;
	Vec2& operator/=(const float rhs);


	float GetLength_sq() const;
	float GetLength() const;

	Vec2 GetNormalized() const;
	Vec2& Normalize();

public:
	float x;
	float y;
};
